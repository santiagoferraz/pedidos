# Banco de Dados:
- O DUMP do banco de dados se encontra na raiz do projeto dentro da pasta DATABASE

# Foram Utilizados:
- Java versão 17
- Maven versão 3.9.6
- MariaDB/MySQL 10.4.32

# Endpount's and Request's:
## Consulta pelo número do pedido - GET
* localhost:8080/ecommerce/pedido?numero_pedido=111

## Consulta pela data do cadastro - GET
* localhost:8080/ecommerce/pedido/2024-04-02

## Consulta de todos os pedidos - GET
* localhost:8080/ecommerce/pedidos

## Salvar pedidos com JSON - POST
* localhost:8080/ecommerce/pedido
```
[
	{
		"numeroControle": 333,
		"dataCadastro": null,
		"nome": "Pirelli",
		"valor": 300.00,
		"quantidade": null,
		"cliente": {
			"id": 7
		}
	},
	{
		"numeroControle": 444,
		"dataCadastro": "2024-04-04",
		"nome": "Firestone",
		"valor": 400.00,
		"quantidade": 6,
		"cliente": {
			"id": 6
		}
	},
	{
		"numeroControle": 555,
		"dataCadastro": "2024-04-05",
		"nome": "Bridgestone",
		"valor": 500.00,
		"quantidade": 10,
		"cliente": {
			"id": 5
		}
	}
]
```

## Salvar pedidos com XML - POST
* localhost:8080/ecommerce/pedido
```
<root>
	<pedido>
		<numeroControle>666</numeroControle>
		<dataCadastro></dataCadastro>
		<nome>Firestone</nome>
		<valor>600</valor>
		<quantidade>6</quantidade>
		<cliente>
			<id>4</id>
		</cliente>
	</pedido>
	<pedido>
		<numeroControle>777</numeroControle>
		<dataCadastro>2024-04-07</dataCadastro>
		<nome>Pirelli</nome>
		<valor>700</valor>
		<quantidade>7</quantidade>
		<cliente>
			<id>3</id>
		</cliente>
	</pedido>
</root>
```
