package com.ecommerce.pedidos.entity;

import com.ecommerce.pedidos.dto.PedidoDTO;
import jakarta.persistence.*;
import lombok.*;

import java.math.BigDecimal;
import java.util.Date;

@Entity
@Table(name = "pedido")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class PedidoEntity {

    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "numero_controle", unique = true, nullable = false)
    private Long numeroControle;

    @Column(name = "data_cadastro", nullable = true)
    private Date dataCadastro;

    @Column(name = "nome", nullable = false)
    private String nome;

    @Column(name = "valor", nullable = false)
    private BigDecimal valor;

    @Column(name = "quantidade", nullable = true)
    private Long quantidade;

    @ManyToOne(optional = true)
    @JoinColumn(name = "codigo_cliente", referencedColumnName = "id")
    private ClienteEntity cliente;

    public Date getDataCadastro() {
        return dataCadastro;
    }

    public void setDataCadastro(Date dataCadastro) {
        this.dataCadastro = dataCadastro;
    }

    public BigDecimal getValor() {
        return valor;
    }

    public void setValor(BigDecimal valor) {
        this.valor = valor;
    }

    public Long getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(Long quantidade) {
        this.quantidade = quantidade;
    }

    public Long getNumeroControle() {
        return numeroControle;
    }

    public void setNumeroControle(Long numeroControle) {
        this.numeroControle = numeroControle;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public ClienteEntity getCliente() {
        return cliente;
    }

    public void setCliente(ClienteEntity cliente) {
        this.cliente = cliente;
    }

    public PedidoEntity(PedidoDTO pedido) {
        this.numeroControle = pedido.numeroControle();
        this.dataCadastro = pedido.dataCadastro();
        this.nome = pedido.nome();
        this.valor = pedido.valor();
        this.quantidade = pedido.quantidade();
        this.cliente = pedido.cliente();
    }

}
