package com.ecommerce.pedidos.service;

import com.ecommerce.pedidos.dto.PedidoDTO;
import com.ecommerce.pedidos.entity.PedidoEntity;
import com.ecommerce.pedidos.repository.PedidoRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.NoSuchElementException;

@Service
@RequiredArgsConstructor
public class PedidoService {

    @Autowired
    private PedidoRepository pedidoRepository;

    public static final Integer QTDE_PEDIDOS_MAX = 10; // Quantidade máxima de pedidos
    public static final Integer DISCOUNT_FOR_MORE_THAN_5_PRODUCTS = 5; // Desconto para mais de 5 produtos
    public static final Integer DISCOUNT_FOR_MORE_THAN_10_PRODUCTS = 10; // Desconto para mais de 10 produtos

    public List<PedidoDTO> getPedidos() {
        List<PedidoDTO> pedidoList;
        pedidoList = pedidoRepository.findAll().stream().map(PedidoDTO::new).toList();
        return pedidoList != null ? pedidoList : null;
    }

    public PedidoDTO getPedidoByNumeroPedido(Long numeroControle) {
        PedidoEntity pedido;
        pedido = pedidoRepository.findByNumeroControle(numeroControle);
        return pedido != null ? new PedidoDTO(pedido) : null;
    }

    public PedidoDTO getPedidoByDataCadastro(String dataCadastro) throws ParseException {
        PedidoEntity pedido;
        Date data = new SimpleDateFormat("yyyy-MM-dd").parse(dataCadastro);
        pedido = pedidoRepository.findByDataCadastro(data);
        return pedido != null ? new PedidoDTO(pedido) : null;
    }

    public void salvarPedido(List<PedidoDTO> pedidoList) {
        if (!this.isValidCount(pedidoList.size())) {
            throw new RuntimeException("A quantidade máxima de pedidos são " + QTDE_PEDIDOS_MAX);
        }

        List<PedidoEntity> pedidoEntityList = pedidoList.stream().map(PedidoEntity::new).toList();

        pedidoEntityList.forEach((p) -> {
            try {
                p.setDataCadastro(this.insertDate(p.getDataCadastro()));
            } catch (ParseException e) {
                throw new RuntimeException(e.getMessage());
            }
            p.setQuantidade(this.insertCountProducts(p.getQuantidade()));
            p.setValor(this.insertDiscount(p.getQuantidade(), p));
            pedidoRepository.save(p);
        });

    }

    private Date insertDate(Date data) throws ParseException {
        if (data == null) {
            return new SimpleDateFormat("yyyy-MM-dd").parse(LocalDate.now().toString());
        } else {
            Calendar cal = Calendar.getInstance();
            cal.setTime(data);
            cal.add(Calendar.DATE, 1);
            return cal.getTime();
        }
    }

    private boolean isValidCount(Integer qtdePedidos) {
        return qtdePedidos <= QTDE_PEDIDOS_MAX;
    }

    private BigDecimal insertDiscount(Long count, PedidoEntity pedido) {
        if (count >= 10) {
            Double valor = pedido.getValor().doubleValue() - ((pedido.getValor().doubleValue() * DISCOUNT_FOR_MORE_THAN_10_PRODUCTS) / 100);
            return new BigDecimal(valor);
        } else if (count > 5) {
            Double valor = pedido.getValor().doubleValue() - ((pedido.getValor().doubleValue() * DISCOUNT_FOR_MORE_THAN_5_PRODUCTS) / 100);
            return new BigDecimal(valor);
        }
        return pedido.getValor();
    }

    private Long insertCountProducts(Long qtde) {
        return qtde == null ? 1 : qtde;
    }

}
