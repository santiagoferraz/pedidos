package com.ecommerce.pedidos.dto;

import com.ecommerce.pedidos.entity.ClienteEntity;
import com.ecommerce.pedidos.entity.PedidoEntity;

import java.math.BigDecimal;
import java.util.Date;

public record PedidoDTO(
        Long numeroControle,
        Date dataCadastro,
        String nome,
        BigDecimal valor,
        Long quantidade,
        ClienteEntity cliente
) {

    public PedidoDTO(PedidoEntity pedido) {
        this(pedido.getNumeroControle(), pedido.getDataCadastro(), pedido.getNome(),
                pedido.getValor(), pedido.getQuantidade(), pedido.getCliente());
    }

}
