package com.ecommerce.pedidos.repository;

import com.ecommerce.pedidos.entity.ClienteEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface ClienteRepository extends JpaRepository<ClienteEntity, Long>  {

    Optional<ClienteEntity> findById(Long id);

}
