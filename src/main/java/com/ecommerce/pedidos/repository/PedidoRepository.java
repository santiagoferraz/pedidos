package com.ecommerce.pedidos.repository;

import com.ecommerce.pedidos.entity.PedidoEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Date;

public interface PedidoRepository extends JpaRepository<PedidoEntity, Long>  {

    PedidoEntity findByNumeroControle(Long numeroControle);

    PedidoEntity findByDataCadastro(Date dataCadastro);

}
