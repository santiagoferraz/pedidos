package com.ecommerce.pedidos.controller;

import com.ecommerce.pedidos.dto.PedidoDTO;
import com.ecommerce.pedidos.service.PedidoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.text.ParseException;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("ecommerce")
public class PedidoController {

    @Autowired
    private PedidoService pedidoService;

    @GetMapping("/pedidos")
    public ResponseEntity<List<PedidoDTO>> getAll() {
        return ResponseEntity.ok(pedidoService.getPedidos());
    }

    @GetMapping("/pedido")
    public ResponseEntity<PedidoDTO> getPedidoByNumeroControle(@RequestParam(name = "numero_pedido") Long numeroControle) {
        return ResponseEntity.ok(pedidoService.getPedidoByNumeroPedido(numeroControle));
    }

    @GetMapping("/pedido/{data_cadastro}")
    public ResponseEntity<PedidoDTO> getPedidoByDataCadastro(@PathVariable("data_cadastro") String dataCadastro) throws ParseException {
        return ResponseEntity.ok(pedidoService.getPedidoByDataCadastro(dataCadastro));
    }

    @PostMapping(value = "/pedido", consumes = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE},
                                    produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
    public void salvarPedido(@RequestBody List<PedidoDTO> pedidoList) {
        pedidoService.salvarPedido(pedidoList);
    }

}
